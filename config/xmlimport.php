<?php

    return [
        'source' => env('XML_IMPORT_SOURCE', storage_path('app/feed.xml')),
        'destination' => env('XML_IMPORT_DESTINATION', 'mysql'),
    ];
