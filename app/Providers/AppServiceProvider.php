<?php

namespace App\Providers;

use App\Contracts\DataStorage;
use App\Services\MysqlDataStorage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataStorage::class, function ($app) {
            $storageType = env('XML_IMPORT_DESTINATION');

            switch ($storageType) {
                case 'mysql':
                    return new MysqlDataStorage();
                // Add more cases for other storage types
                default:
                    throw new \InvalidArgumentException("Invalid storage type: {$storageType}");
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
