<?php

namespace App\Console\Commands;

use App\Contracts\DataStorage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class XmlToDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xml:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from XML file to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(DataStorage $storage)
    {
        try {
            $xmlFilePath = env('XML_IMPORT_SOURCE');
            $xml = simplexml_load_file($xmlFilePath);

            foreach ($xml->item as $item) {
                $this->insertDataIntoStorage($item, $storage);
            }

            $this->info('Data imported successfully.');
        } catch (\Exception $e) {
            // Display an error message to the console
            $this->error($e->getMessage());

            // Log the error message
            Log::error('An error occurred: ' . $e->getMessage());
        }
    }

    /**
     * @param $item
     * @param DataStorage $storage
     */
    private function insertDataIntoStorage($item, DataStorage $storage)
    {
        $data = [
            'entity_id' => (string)$item->entity_id,
            'category_name' => (string)$item->CategoryName,
            'sku' => (string)$item->sku,
            'attributes' => json_encode($this->extractDynamicAttributes($item)),
        ];

        $storage->insert($data);
    }

    /**
     * @param $item
     * @return array
     */
    private function extractDynamicAttributes($item)
    {
        $dynamicAttributes = [];

        // Extract dynamic attributes from the XML
        foreach ($item->children() as $key => $value) {
            if (!in_array($key, ['entity_id', 'CategoryName', 'sku'])) {
                $dynamicAttributes[$key] = (string)$value;
            }
        }

        return $dynamicAttributes;
    }
}
