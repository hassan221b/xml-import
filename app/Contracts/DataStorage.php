<?php

namespace App\Contracts;

interface DataStorage
{
    /**
     * @param array $data
     * @return mixed
     */
    public function insert(array $data);
}

