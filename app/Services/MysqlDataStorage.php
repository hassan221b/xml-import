<?php

namespace App\Services;

use App\Contracts\DataStorage;
use App\Models\Product;

class MysqlDataStorage implements DataStorage
{
    /**
     * @param array $data
     */
    public function insert(array $data)
    {
        Product::create($data);
    }
}
