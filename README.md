# XML Import Project README

## Introduction

This document provides instructions about the XML import application built in Laravel.

## Prerequisites

Before testing the command, make sure you have the following prerequisites installed on your system:

- Laravel (version 10.x)
- Composer
- PHP
- Mysql

## Installation

1. **Clone the repository:**

    ```bash
    git clone https://gitlab.com/hassan221b/xml-import.git
    ```

2. **Navigate to the project directory:**

    ```bash
    cd xml-import
    ```

3. **Install dependencies:**

    ```bash
    composer install
    ```

4. **Set up your environment file:**

    ```bash
    cp .env.example .env
    ```

    Update the `.env` file with your configuration settings, including database credentials.

5. **Generate application key:**

    ```bash
    php artisan key:generate
    ```

6. **Run database migrations:**

    ```bash
    php artisan migrate
    ```

7. **Run xml:import command to feed the xml from source to the destination db:**

    ```bash
    php artisan xml:import
    ```
